import java.util.*

class MyCafe {

    private val coffeeList = mutableListOf(
        Coffee("Espresso", 5),
        Coffee("Short Macchiato", 6),
        Coffee("Cappuccino", 7),
        Coffee("Latte", 7),
        Coffee("Hot Chocolate", 3),
        Coffee("Affogato", 9),
        Coffee("Caramel Latte", 6)
    )

    fun printMenu() {
        coffeeList.forEach { println("${it.name} : ${it.price}") }
    }

    fun addMenu() {
        val scanner = Scanner(System.`in`)
        println("Please enter coffee name and price : ")
        val menuName = scanner.next()
        val menuPrice = scanner.nextInt()
        scanner.close()
        coffeeList.add(Coffee(menuName, menuPrice))
    }

}
fun main() {
    val cafe = MyCafe()
    cafe.addMenu()
    cafe.printMenu()
}